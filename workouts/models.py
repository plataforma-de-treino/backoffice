# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models

# Create your models here.
class Level(models.Model):
    name = models.CharField(max_length=30)
    description = models.CharField(max_length=300)

    def __str__(self):
        return "%s" % (self.name)

    def __unicode__(self):  # __unicode__ on Python 2
        return "%s" % (self.name)

    class Meta:
        ordering = ('name',)

class Place(models.Model):
    name = models.CharField(max_length=30)
    description = models.CharField(max_length=300)

    def __str__(self):
        return "%s" % (self.name)

    def __unicode__(self):  # __unicode__ on Python 2
        return "%s" % (self.name)

    class Meta:
        ordering = ('name',)

class Category(models.Model):
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=300)

    def __str__(self):
        return "%s" % (self.name)

    def __unicode__(self):  # __unicode__ on Python 2
        return "%s" % (self.name)

    class Meta:
        ordering = ('name',)

class Exercise(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=300, null=True)
    category = models.ForeignKey(Category, null=True, on_delete=models.CASCADE)
    level = models.ForeignKey(Level, null=True, on_delete=models.CASCADE)
    place = models.ForeignKey(Place, null=True, on_delete=models.CASCADE)
    video = models.CharField(max_length=300, null=True)

    def __str__(self):
        return "Nome: %s | local: %s | Nível: %s" % (self.name,self.place,self.level)

    def __unicode__(self):  # __unicode__ on Python 2
        return "Nome: %s | local: %s | Nível: %s" % (self.name,self.place,self.level)

    class Meta:
        ordering = ('name',)

class Routine(models.Model):
    name = models.CharField(max_length=100)
    exercise = models.OneToOneField(Exercise)
    repetition = models.PositiveSmallIntegerField()
    weight = models.DecimalField(max_digits=5, decimal_places=2)
    rest = models.DurationField(help_text="<em>hh:mm:ss</em>.")
    duration = models.DurationField(help_text="<em>hh:mm:ss</em>.")

    def __str__(self):
        return "%s" % (self.name)

    def __unicode__(self):  # __unicode__ on Python 2
        return "%s" % (self.name)

    class Meta:
        ordering = ('name',)

class Training(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=300, null=True)
    routines = models.ManyToManyField(Routine)
    level = models.ForeignKey(Level, null=True, on_delete=models.CASCADE)
    place = models.ForeignKey(Place, null=True, on_delete=models.CASCADE)

    def __str__(self):
        return "%s" % (self.name)

    def __unicode__(self):  # __unicode__ on Python 2
        return "%s" % (self.name)

    class Meta:
        ordering = ('name',)
