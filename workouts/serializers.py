# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from rest_framework import serializers
from workouts.models import Training, Routine, Exercise, Category, Place, Level

# Level methods
class LevelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Level
        fields = ('id', 'name', 'description')

# Place methods
class PlaceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Place
        fields = ('id', 'name', 'description')

# Category methods
class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('id', 'name', 'description')

# Exercise methods
class ExerciseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Exercise
        fields = ('id', 'name', 'description', 'category', 'level', 'place','video')

# Routine methods
class RoutineSerializer(serializers.ModelSerializer):
    class Meta:
        model = Routine
        fields = ('id', 'name', 'exercise', 'repetition', 'weight', 'rest', 'duration')

# Training methods
class TrainingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Training
        fields = ('id', 'name', 'description', 'routines', 'level', 'place')
