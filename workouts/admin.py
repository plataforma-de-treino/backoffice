# -*- coding: utf-8 -*-
from django.contrib import admin

# Register your models here.
from .models import Level
admin.site.register(Level)

from .models import Place
admin.site.register(Place)

from .models import Category
admin.site.register(Category)

from .models import Exercise
admin.site.register(Exercise)

from .models import Routine
admin.site.register(Routine)

from .models import Training
admin.site.register(Training)
