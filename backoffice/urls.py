"""backoffice URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from workouts import views

urlpatterns = [
    url(r'^levels/$', views.level_list),
    url(r'^levels/(?P<pk>[0-9]+)/$', views.level_detail),
    url(r'^places/$', views.place_list),
    url(r'^places/(?P<pk>[0-9]+)/$', views.place_detail),
    url(r'^categories/$', views.category_list),
    url(r'^categories/(?P<pk>[0-9]+)/$', views.category_detail),
    url(r'^exercises/$', views.exercise_list),
    url(r'^exercises/(?P<pk>[0-9]+)/$', views.exercise_detail),
    url(r'^routines/$', views.routine_list),
    url(r'^routines/(?P<pk>[0-9]+)/$', views.routine_detail),
    url(r'^trainings/$', views.training_list),
    url(r'^trainings/(?P<pk>[0-9]+)/$', views.training_detail),
    url(r'^admin/', admin.site.urls),
    #url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]
